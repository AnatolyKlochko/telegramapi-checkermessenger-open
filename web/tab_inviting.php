<?php if (! defined('ROOT')) exit; ?>
<div class="row">
    <div class="col-md-6">
        <div class="row form-group">
            <div class="col-12 col-md-12 text-left"><label for="textarea-input" class=" form-control-label">Message</label></div>
            <div class="col-12 col-md-12"><textarea name="message" id="tab-inviting-message" rows="9" placeholder="An invite content..." class="form-control" maxlength="4096" required></textarea></div>
        </div>
        <div class="row mt-3">
            <div class="col-md-5">
                <div id="tab-inviting-process">
                    <div class="row">
                        <div class="col col-md-4 text-left">info:</div>
                        <div class="col col-md-8 text-left text-primary" id="info"></div>
                    </div>
                    <div class="row">
                        <div class="col col-md-4 text-left">action:</div>
                        <div class="col col-md-8 text-left text-secondary" id="action"></div>
                    </div>
                    <div class="row">
                        <div class="col col-md-4 text-sm-left">started:</div>
                        <div class="col col-md-8 text-left text-secondary" id="start"></div>
                    </div>
                    <div class="row">
                        <div class="col col-md-4 text-left">in:</div>
                        <div class="col col-md-8 text-left text-primary"><span id="passed"></span></div>
                    </div>
                    <div class="row">
                        <div class="col col-md-4 text-left">finished:</div>
                        <div class="col col-md-8 text-left text-secondary" id="end"></div>
                    </div>
                    <div class="row">
                        <div class="col col-md-4 text-left">total:</div>
                        <div class="col col-md-8 text-left text-secondary"><span id="total"></span></div>
                    </div>
                    <div class="row">
                        <div class="col col-md-4 text-left">current:</div>
                        <div class="col col-md-8 text-left text-primary"><span id="current"></span></div>
                    </div>
                    <div class="row">
                        <div class="col col-md-4 text-left">left:</div>
                        <div class="col col-md-8 text-left text-secondary"><span id="left"></span></div>
                    </div>
                </div>
            </div>
            <div id="tab-inviting-controls" class="col-md-7">
                <div class="text-right">
                    <span class="btn btn-secondary btn-sm" id="tab-inviting-btn-stop">Stop</span>
                    <span class="btn btn-primary btn-sm" id="tab-inviting-btn-start">Start</span>
                </div>
            </div>
        </div>  
    </div>
    <div class="col-md-6">
        <div class="col-12 col-md-12 text-left"><label for="tab-inviting-phoneslist-datatable" class="form-control-label">Choose List(s)</label></div>
        <table id="tab-inviting-phoneslist-datatable" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Telegram</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
