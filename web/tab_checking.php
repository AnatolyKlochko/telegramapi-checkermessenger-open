<?php if (! defined('ROOT')) exit; ?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <table id="tab-checking-phoneslist-datatable" class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Name</th>
                      <th>Unique</th>
                      <th>Total</th>
                      <th>Uploaded</th>
                      <th>Is Telegram Checked</th>
                      <th>Checked Date</th>
                      <th>Accounts</th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-md-6">
        <div id="tab-checking-process">
            <div class="row">
                <div class="col col-md-3 text-left">info:</div>
                <div class="col col-md-9 text-left text-primary" id="info"></div>
            </div>
            <div class="row">
                <div class="col col-md-3 text-left">action:</div>
                <div class="col col-md-9 text-left text-secondary" id="action"></div>
            </div>
            <div class="row">
                <div class="col col-md-3 text-sm-left">started:</div>
                <div class="col col-md-9 text-left text-secondary" id="start"></div>
            </div>
            <div class="row">
                <div class="col col-md-3 text-left">in:</div>
                <div class="col col-md-9 text-left text-primary"><span id="passed"></span></div>
            </div>
            <div class="row">
                <div class="col col-md-3 text-left">finished:</div>
                <div class="col col-md-9 text-left text-secondary" id="end"></div>
            </div>
            <div class="row">
                <div class="col col-md-3 text-left">total:</div>
                <div class="col col-md-9 text-left text-secondary"><span id="total"></span></div>
            </div>
            <div class="row">
                <div class="col col-md-3 text-left">current:</div>
                <div class="col col-md-9 text-left text-primary"><span id="current"></span></div>
            </div>
            <div class="row">
                <div class="col col-md-3 text-left">left:</div>
                <div class="col col-md-9 text-left text-secondary"><span id="left"></span></div>
            </div>
            <div class="row">
                <div class="col col-md-3 text-left">Telegram Accounts:</div>
                <div class="col col-md-9 text-left text-primary"><span id="telegram"></span></div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div id="tab-checking-controls" class="row">
            <div class="col text-right">
                <span class="btn btn-outline-dark btn-sm" id="tab-checking-btn-unckecked">Unchecked</span>
                <span class="btn btn-outline-success btn-sm" id="tab-checking-btn-reload">Reload</span>
                <span class="btn btn-primary btn-sm" id="tab-checking-btn-start">Start</span>
            </div>
        </div>
    </div>
    
</div>