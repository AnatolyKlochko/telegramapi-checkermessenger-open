<?php if (! defined('ROOT')) exit; ?>
<form role="form" action="" method="post" class="f1" id="tab-login-form">
    <h3>Enter in Telegram</h3>
    <p id="tab-login-monitor">Request for Login Code may take some time (up to 2 min)...</p>
    <div class="f1-steps">
            <div class="f1-progress">
                <div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3" style="width: 16.66%;"></div>
            </div>
            <div class="f1-step active">
                    <div class="f1-step-icon"><i class="fa fa-phone"></i></div>
                    <p>Phone Number</p>
            </div>
            <div class="f1-step">
                    <div class="f1-step-icon"><i class="fa fa-mobile-phone"></i></div>
                    <p>Login Code</p>
            </div>
            <div class="f1-step">
                    <div class="f1-step-icon"><i class="fa fa-unlock-alt"></i></div>
                    <p>Account</p>
            </div>
    </div>

    <fieldset>
        <h4>Telegram account:</h4>
        <div class="form-group">
            <label class="sr-only" for="f1-phone-number">Phone number</label>
            <!--input type="text" name="f1-phone-number" placeholder="Phone number" class="f1-phone-number form-control" id="f1-phone-number"-->
            <select  class="f1-phone-number form-control" id="phone-number">
                <option value="" disabled selected>Select phone number</option>
                <?php 
                $phones = getAccountPhones();
                foreach ($phones as $phone) {?>
                <option value="<?= $phone ?>" <?= getCurrentPhone() === $phone ? 'selected' : '' ?>>+<?= $phone ?></option><?php
                }
                ?>
            </select>
        </div>
        <div class="form-group img-process" id="tab-login-img-process-login-code">
            <img src="images/process-circle-blue.gif" />
        </div>
        <div class="f1-buttons">
            <button type="button" class="btn btn-next" style="background: #bbb;">Skip</button>
            <button type="button" class="btn btn-next" id="request-code_">Login Code</button>
            <button type="button" class="btn btn-primary" id="request-code">Login Code</button>
        </div>
    </fieldset>

    <fieldset>
        <h4>Enter login code:</h4>
        <div class="form-group">
            <label class="sr-only" for="f1-login-code">Login code</label>
            <input type="text" name="f1-login-code" placeholder="Login code" class="f1-login-code form-control" id="login-code">
        </div>
        <div class="form-group img-process" id="tab-login-img-process-login">
            <img src="images/process-circle-blue.gif" />
        </div>
        
        <div class="f1-buttons">
            <button type="button" class="btn btn-previous">Previous</button>
            <button type="button" class="btn btn-next" id="login_">Login</button>
            <button type="button" class="btn btn-primary" id="login">Login</button>
        </div>
    </fieldset>
    
    <fieldset>
        <h4></h4>
        <div class="form-group img-process" style="display: block;">
            <img src="images/logging-success.jpg" />
        </div>
                
        <div class="f1-buttons">
            <button type="button" class="btn btn-previous">Previous</button>
        </div>
    </fieldset>

</form>