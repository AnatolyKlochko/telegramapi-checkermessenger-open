<?php if (! defined('ROOT')) exit; ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Telegram Plugin</title>
        <meta name="description" content="Telegram Plugin - API Implementation">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link href="<?= URIPREF ?>/images/Telegram-icon.png" rel="shortcut icon">
                
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.3/normalize.min.css" />
        <link rel="stylesheet" 
            href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" 
            integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" 
            crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="<?= URIPREF ?>/assets/css/themify-icons.css">
        <link rel="stylesheet" href="<?= URIPREF ?>/assets/css/flag-icon.min.css">
        <link rel="stylesheet" href="<?= URIPREF ?>/assets/css/cs-skin-elastic.css">
        <!-- <link rel="stylesheet" href="<?= URIPREF ?>/assets/css/bootstrap-select.less"> -->
        <!-- Admin theme 'Sufee-Admin' -->
        <link rel="stylesheet" href="<?= URIPREF ?>/assets/scss/Sufee-Admin-style.css">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
        <!-- Form wizard 'bootzard-bootstrap-wizard' -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="<?= URIPREF ?>/assets/css/bootzard-bootstrap-wizard-form-elements.css">
        <link rel="stylesheet" href="<?= URIPREF ?>/assets/css/bootzard-bootstrap-wizard-style.css">
        
        <!-- Bootstrap DataTable -->
        <!--link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css"-->
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap4.min.css">
	            
        <!-- Styles -->
        <link rel="stylesheet" href="<?= URIPREF ?>/styles.css">
        <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
    </head>
    <body>
        <div class="content mt-3">
            <!--div class="animated fadeIn"-->
                
                <!-- Settings -->
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8">
                        <div class="text-right">
                            <span id="settings" class="btn btn-primary btn-sm mb-1" data-toggle="modal" data-target="#part-settings">Settings</span>
                            <span id="logout" class="btn btn-secondary btn-sm mb-1">Logout</span>
                        </div>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
                
                <!-- Tabs -->
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-header">
                                <h4>Telegram</h4>
                            </div>
                            <div class="card-body">
                                <div class="custom-tab">
                                    <nav>
                                        <div class="nav nav-tabs" id="nav-main-tab" role="tablist">
                                            <a class="nav-item nav-link active" id="custom-nav-login-tab" data-toggle="tab" href="#custom-nav-login" role="tab" aria-controls="custom-nav-login" aria-selected="true">Login</a>
                                            <a class="nav-item nav-link" id="custom-nav-upload-tab" data-toggle="tab" href="#custom-nav-upload" role="tab" aria-controls="custom-nav-upload" aria-selected="false">Upload</a>
                                            <a class="nav-item nav-link" id="custom-nav-data-tab" data-toggle="tab" href="#custom-nav-data" role="tab" aria-controls="custom-nav-data" aria-selected="false">Data</a>
                                            <a class="nav-item nav-link" id="custom-nav-check-tab" data-toggle="tab" href="#custom-nav-check" role="tab" aria-controls="custom-nav-check" aria-selected="false">Checking</a>
                                            <a class="nav-item nav-link" id="custom-nav-inviting-tab" data-toggle="tab" href="#custom-nav-inviting" role="tab" aria-controls="custom-nav-inviting" aria-selected="false">Inviting</a>
                                            <a class="nav-item nav-link" id="custom-nav-messaging-tab" data-toggle="tab" href="#custom-nav-messaging" role="tab" aria-controls="custom-nav-messaging" aria-selected="false">Messaging</a>
                                            <a class="nav-item nav-link" id="custom-nav-notes-tab" data-toggle="tab" href="#custom-nav-notes" role="tab" aria-controls="custom-nav-notes" aria-selected="false">Notes</a>
                                        </div>
                                    </nav>
                                    <div class="tab-content pt-2" id="nav-main-tabContent">
                                        <div class="tab-pane fade show active" id="custom-nav-login" role="tabpanel" aria-labelledby="custom-nav-login-tab">
                                            <?php include './tab_login.php' ?>
                                        </div>
                                        <div class="tab-pane fade" id="custom-nav-upload" role="tabpanel" aria-labelledby="custom-nav-upload-tab">
                                            <?php include './tab_upload.php' ?>
                                        </div>
                                        <div class="tab-pane fade" id="custom-nav-data" role="tabpanel" aria-labelledby="custom-nav-data-tab">
                                            <?php include './tab_data.php' ?>
                                        </div>
                                        <div class="tab-pane fade" id="custom-nav-check" role="tabpanel" aria-labelledby="custom-nav-check-tab">
                                            <?php include './tab_checking.php' ?>
                                        </div>
                                        <div class="tab-pane fade" id="custom-nav-inviting" role="tabpanel" aria-labelledby="custom-nav-inviting-tab">
                                            <?php include './tab_inviting.php' ?>
                                        </div>
                                        <div class="tab-pane fade" id="custom-nav-messaging" role="tabpanel" aria-labelledby="custom-nav-messaging-tab">
                                            <?php include './tab_messaging.php' ?>
                                        </div>
                                        <div class="tab-pane fade" id="custom-nav-notes" role="tabpanel" aria-labelledby="custom-nav-notes-tab">
                                            <p>developing stage...</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
                                
                
                <!-- Hidden Modal: 'Settings' -->
                <div class="hidden">
                    <div class="modal fade" id="part-settings" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <!--h5 class="modal-title" id="largeModalLabel">Large Modal</h5-->
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <?php include './part_settings.php' ?>
                                </div>
                                <!--div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                    <button type="button" class="btn btn-primary">Confirm</button>
                                </div-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Hidden Modal: 'Settings'/'Telegram Accounts' Add -->
                <div class="hidden">
                    <span id="telegramaccounts-modal-add-btn-control" class="hidden" data-toggle="modal" data-target="#telegramaccounts-modal-add"></span>
                    <div class="modal fade" id="telegramaccounts-modal-add" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-sm" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="largeModalLabel">Add Account</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="row form-group">
                                        <div class="col-12"><input type="text" data-attr="phone" id="telegramaccounts-modal-add-phone" placeholder="Phone" class="form-control"></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-12"><input type="text" data-attr="username" id="telegramaccounts-modal-add-username" placeholder="@username" class="form-control"></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-12"><input type="text" data-attr="api_id" id="telegramaccounts-modal-add-api-id" placeholder="ApiID (api_id)" class="form-control"></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-12"><input type="text" data-attr="api_hash" id="telegramaccounts-modal-add-api-hash" placeholder="ApiHASH (api_hash)" class="form-control"></div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                    <button type="button" id="telegramaccounts-modal-btn-add" class="btn btn-primary">Add</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Hidden Modal: 'Settings'/'Telegram Accounts' Update -->
                <div class="hidden">
                    <span id="telegramaccounts-modal-update-btn-control" class="hidden" data-toggle="modal" data-target="#telegramaccounts-modal-update"></span>
                    <div class="modal fade" id="telegramaccounts-modal-update" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-sm" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="largeModalLabel">Update Account</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="row form-group">
                                        <div class="col-12"><input type="text" data-attr="phone" id="telegramaccounts-modal-update-phone" placeholder="Phone" class="form-control"></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-12"><input type="text" data-attr="username" id="telegramaccounts-modal-update-username" placeholder="@username" class="form-control"></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-12"><input type="text" data-attr="api_id" id="telegramaccounts-modal-update-api-id" placeholder="ApiID (api_id)" class="form-control"></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-12"><input type="text" data-attr="api_hash" id="telegramaccounts-modal-update-api-hash" placeholder="ApiHASH (api_hash)" class="form-control"></div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                    <button type="button" id="telegramaccounts-modal-btn-update" class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Hidden Modal: 'Settings'/'Telegram Accounts' Delete -->
                <div class="hidden">
                    <span id="telegramaccounts-modal-delete-btn-control" class="hidden" data-toggle="modal" data-target="#telegramaccounts-modal-delete"></span>
                    <div class="modal fade" id="telegramaccounts-modal-delete" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-sm" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="largeModalLabel">Delete Account</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="row form-group">
                                        <div class="col-12"><input type="text" data-attr="phone" id="telegramaccounts-modal-delete-phone" placeholder="Phone" class="form-control" disabled></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-12"><input type="text" data-attr="username" id="telegramaccounts-modal-delete-username" placeholder="@username" class="form-control" disabled></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-12"><input type="text" data-attr="api_id" id="telegramaccounts-modal-delete-api-id" placeholder="ApiID (api_id)" class="form-control" disabled></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-12"><input type="text" data-attr="api_hash" id="telegramaccounts-modal-delete-api-hash" placeholder="ApiHASH (api_hash)" class="form-control" disabled></div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                    <button type="button" id="telegramaccounts-modal-btn-delete" class="btn btn-primary">Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Hidden Modal: 'Settings'/'Application Users' Add -->
                <div class="hidden">
                    <span id="appusers-modal-add-btn-control" class="hidden" data-toggle="modal" data-target="#appusers-modal-add"></span>
                    <div class="modal fade" id="appusers-modal-add" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-sm" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="largeModalLabel">Add User</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="row form-group">
                                        <div class="col-12"><input type="text" data-attr="login" id="appusers-modal-add-login" placeholder="Login" class="form-control"></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-12"><input type="text" data-attr="password" id="appusers-modal-add-password" placeholder="Password" class="form-control"></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-12"><input type="text" data-attr="salt" id="appusers-modal-add-salt" placeholder="Salt" class="form-control"></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-12">
                                            <select data-attr="account" id="appusers-modal-add-account" class="form-control-lg form-control">
                                                <option value="">Telegram Account Phone</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                    <button type="button" id="appusers-modal-btn-add" class="btn btn-primary">Add</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Hidden Modal: 'Settings'/'Application Users' Update -->
                <div class="hidden">
                    <span id="appusers-modal-update-btn-control" class="hidden" data-toggle="modal" data-target="#appusers-modal-update"></span>
                    <div class="modal fade" id="appusers-modal-update" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-sm" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="largeModalLabel">Update User</h5>
                                    <button type="button" id="appusers-modal-update-btn-close" class="close" data-dismiss="modal" aria-label="Close">
                                        <span id="appusers-modal-update-close" aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="row form-group">
                                        <div class="col-12"><input type="text" data-attr="login" id="appusers-modal-update-login" placeholder="Login" class="form-control"></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-12"><input type="text" data-attr="password" id="appusers-modal-update-password" placeholder="Password" class="form-control"></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-12"><input type="text" data-attr="salt" id="appusers-modal-update-salt" placeholder="Salt" class="form-control"></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-12">
                                            <select id="appusers-modal-update-account" data-attr="account" class="form-control-lg form-control">
                                                <option value="">Telegram Account Phone</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                    <button type="button" id="appusers-modal-btn-update" class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Hidden Modal: 'Settings'/'Application Users' Delete -->
                <div class="hidden">
                    <span id="appusers-modal-delete-btn-control" class="hidden" data-toggle="modal" data-target="#appusers-modal-delete"></span>
                    <div class="modal fade" id="appusers-modal-delete" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-sm" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="largeModalLabel">Delete User</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="row form-group">
                                        <div class="col-12"><input type="text" data-attr="login" id="appusers-modal-delete-login" placeholder="Login" class="form-control" disabled></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-12"><input type="text" data-attr="password" id="appusers-modal-delete-password" placeholder="Password" class="form-control" disabled></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-12"><input type="text" data-attr="salt" id="appusers-modal-delete-salt" placeholder="Salt" class="form-control" disabled></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-12">
                                            <select data-attr="account" id="appusers-modal-delete-account" class="form-control-lg form-control" disabled>
                                                <option value="0">Telegram Account Phone</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                    <button type="button" id="appusers-modal-btn-delete" class="btn btn-primary">Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                                
                
                <!-- Hidden Modal: 'Exception' -->
                <div class="hidden">
                <!-- Modal -->
                <span id="btn-exception-modal" class="hidden" data-toggle="modal" data-target="#exception-modal"></span>
                <div class="modal fade" id="exception-modal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="largeModalLabel">Exception</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body text-justify" style="word-wrap: break-word;">
                                <div class="row form-group">
                                    <div class="col-12 font-weight-bold">Message:</div>
                                    <div id="exception-modal-message" class="col-12"></div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-12 font-weight-bold">Additional (original message):</div>
                                    <div id="exception-modal-data" class="col-12"></div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Ok</button>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                
                <div class="row mb-5"></div>
                <div class="row mb-5"></div>
                <div class="row mb-5"></div>
            <!--/div-->
        </div>
               
        <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous">
        </script>
        <script 
            src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" 
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" 
            crossorigin="anonymous">
        </script>
        <script 
            src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" 
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" 
            crossorigin="anonymous">
        </script>
        <script src="<?= URIPREF ?>/assets/js/plugins.js"></script>
        <script src="<?= URIPREF ?>/assets/js/Sufee-Admin-main.js"></script>
               
        <!-- DataTable -->
        <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-1.12.4.js">
	</script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap4.min.js">
	</script>
        <!--script src="<?= URIPREF ?>/assets/js/lib/data-table/datatables.min.js"></script-->
        <!--script src="<?= URIPREF ?>/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script-->
        <!--script src="<?= URIPREF ?>/assets/js/lib/data-table/dataTables.buttons.min.js"></script-->
        <!--script src="<?= URIPREF ?>/assets/js/lib/data-table/buttons.bootstrap.min.js"></script-->
                
        
        <script src="<?= URIPREF ?>/assets/js/lib/data-table/jszip.min.js"></script>
        <script src="<?= URIPREF ?>/assets/js/lib/data-table/pdfmake.min.js"></script>
        <script src="<?= URIPREF ?>/assets/js/lib/data-table/vfs_fonts.js"></script>
        <script src="<?= URIPREF ?>/assets/js/lib/data-table/buttons.html5.min.js"></script>
        <script src="<?= URIPREF ?>/assets/js/lib/data-table/buttons.print.min.js"></script>
        <script src="<?= URIPREF ?>/assets/js/lib/data-table/buttons.colVis.min.js"></script>
        <script src="<?= URIPREF ?>/assets/js/lib/data-table/datatables-init.js"></script>
                
        <!-- Form wizard 'bootzard-bootstrap-wizard' 
            Be aware that 'DataTable' Bootstrap owerrides some variables and blocked 'bootzard-bootstrap-wizard'. 
            By this cause need locate 'bootzard-bootstrap-wizard' scripts AFTER 'DataTable' scripts.
        -->
        <script src="<?= URIPREF ?>/assets/js/jquery.backstretch.min.js"></script>
        <script src="<?= URIPREF ?>/assets/js/retina-1.1.0.min.js"></script>
        <script src="<?= URIPREF ?>/assets/js/bootzard-bootstrap-wizard-scripts.js"></script>
        
        
        <script>
            "use strict"
            var uri_pref = '<?= URIPREF ?>'; // nice WP trick    
            //let m = `${uri_pref}`;//console.log(m);
        </script>
        <script src="<?= URIPREF ?>/script.js"></script>
    </body>
</html>