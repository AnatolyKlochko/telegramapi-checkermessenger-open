<?php if (! defined('ROOT')) exit; ?>
<div class="row">
    <div class="col">
        <table id="tab-data-phones-datatable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th></th>
              <th>Number</th>
              <th>Is Checked</th><!-- IsTelegramChecked -->
              <th>Checked Date</th><!-- TelegramCheckedDate -->
              <th>Telegram</th><!-- HasTelegramAccount -->
            </tr>
          </thead>
          <tbody></tbody>
        </table>
    </div>
</div>
<div id="tab-data-phones-controls" class="row mt-3">
    <div class="col text-right">
        <span class="btn btn-secondary btn-sm" id="tab-data-phones-btn-delete" data-toggle="modal" data-target="#data-phones-modal-delete">Delete</span>
        <span class="btn btn-outline-success btn-sm" id="tab-data-phones-btn-reload">Reload</span>
        <span class="btn btn-outline-primary btn-sm" id="tab-data-phones-btn-filter">Filter</span>
    </div>
</div>