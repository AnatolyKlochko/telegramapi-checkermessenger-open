<?php if (! defined('ROOT')) exit; ?>
<!--div id="wrapper">
    <div class="scrollbar" id="style-4">
    <div class="force-overflow"-->

        <div class="card-body card-block" style="padding: 0px 0px 0px 0px;">
            <div action="" method="post" enctype="multipart/form-data" class="form-horizontal" id="">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header pl-3 pt-0 pb-0 text-left">
                                <strong class="card-title">Uploading (Phone Lists)</strong>
                            </div>
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col col-md-6">
                                        <div class="p-0 text-primary text-left">Parsing</div>
                                        <div class="row">
                                            <div class="col col-md-6">
                                                <div class="row form-group">
                                                    <div class="col col-md-3"><label for="uploading-parsing-delay-min" class=" form-control-label">Delay Min</label></div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="text" id="uploading-parsing-delay-min" name="uploading-parsing-delay-min" placeholder="Minimal delay" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col col-md-3"><label for="uploading-parsing-packet-min" class=" form-control-label">Packet Min</label></div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="text" id="uploading-parsing-packet-min" name="uploading-parsing-packet-min" placeholder="Minimal Packet" class="form-control" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col col-md-6">
                                                <div class="row form-group">
                                                    <div class="col col-md-3"><label for="uploading-parsing-delay-max" class=" form-control-label">Delay Max</label></div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="text" id="uploading-parsing-delay-max" name="uploading-parsing-delay-max" placeholder="Maximum delay" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col col-md-3"><label for="uploading-parsing-packet-max" class=" form-control-label">Packet Max</label></div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="text" id="uploading-parsing-packet-max" name="uploading-parsing-packet-max" placeholder="Maximum packet" class="form-control" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col col-md-6">
                                        <div class="p-0 text-primary text-left">Writing</div>
                                        <div class="row">
                                            <div class="col col-md-6">
                                                <div class="row form-group">
                                                    <div class="col col-md-3"><label for="uploading-writing-delay-min" class=" form-control-label">Delay Min</label></div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="text" id="uploading-writing-delay-min" name="uploading-writing-delay-min" placeholder="Minimal delay" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col col-md-3"><label for="uploading-writing-packet-min" class=" form-control-label">Packet Min</label></div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="text" id="uploading-writing-packet-min" name="uploading-writing-packet-min" placeholder="Minimal Packet" class="form-control" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col col-md-6">
                                                <div class="row form-group">
                                                    <div class="col col-md-3"><label for="uploading-writing-delay-max" class=" form-control-label">Delay Max</label></div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="text" id="uploading-writing-delay-max" name="uploading-writing-delay-max" placeholder="Maximum delay" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col col-md-3"><label for="uploading-writing-packet-max" class=" form-control-label">Packet Max</label></div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="text" id="uploading-writing-packet-max" name="uploading-writing-packet-max" placeholder="Maximum packet" class="form-control" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header pl-3 pt-0 pb-0 text-left">
                                <strong class="card-title">Checking (Phones on Telegram Accounts)</strong>
                            </div>
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col col-md-6">
                                        <div class="p-0 text-primary text-left">Importing (Contacts to Telegram)</div>
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="checking-importing-fnprefix" class=" form-control-label">First Name Prefix</label></div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" id="checking-importing-fnprefix" name="checking-importing-fnprefix" placeholder="First Name Prefix" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col col-md-6">                               
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="checking-importing-delay-min" class="form-control-label">Delay Min</label>
                                                    </div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="text" id="checking-importing-delay-min" name="checking-importing-delay-min" placeholder="Minimal delay" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col col-md-3"><label for="checking-importing-packet-min" class=" form-control-label">Packet Min</label></div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="text" id="checking-importing-packet-min" name="checking-importing-packet-min" placeholder="Minimal Packet" class="form-control" required>
                                                    </div>
                                                </div>            
                                            </div>
                                            <div class="col col-md-6">    
                                                <div class="row form-group">
                                                    <div class="col col-md-3"><label for="checking-importing-delay-max" class=" form-control-label">Delay Max</label></div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="text" id="checking-importing-delay-max" name="checking-importing-delay-max" placeholder="Maximum delay" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col col-md-3"><label for="checking-importing-packet-max" class=" form-control-label">Packet Max</label></div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="text" id="checking-importing-packet-max" name="checking-importing-packet-max" placeholder="Maximum packet" class="form-control" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col col-md-6">
                                        <div class="p-0 text-primary text-left">Deleting</div>
                                        
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="checking-deleting-threshold" class=" form-control-label">Threshold</label></div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" id="checking-deleting-threshold" name="checking-deleting-threshold" placeholder="Packet size" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col col-md-6">
                                                
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="checking-deleting-delay-min" class="form-control-label">Delay Min</label>
                                                    </div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="text" id="checking-deleting-delay-min" name="checking-deleting-delay-min" placeholder="Minimal delay" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col col-md-3"><label for="checking-deleting-packet-min" class=" form-control-label">Packet Min</label></div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="text" id="checking-deleting-packet-min" name="checking-deleting-packet-min" placeholder="Minimal Packet" class="form-control" required>
                                                    </div>
                                                </div>            
                                            </div>
                                            <div class="col col-md-6">    
                                                <div class="row form-group">
                                                    <div class="col col-md-3"><label for="checking-deleting-delay-max" class=" form-control-label">Delay Max</label></div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="text" id="checking-deleting-delay-max" name="checking-deleting-delay-max" placeholder="Maximum delay" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col col-md-3"><label for="checking-deleting-packet-max" class=" form-control-label">Packet Max</label></div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="text" id="checking-deleting-packet-max" name="checking-deleting-packet-max" placeholder="Maximum packet" class="form-control" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col col-md-12">
                                                
                                                
                                            </div>
                                        </div>
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row mt-2">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header pl-3 pt-0 pb-0 text-left">
                                <strong class="card-title">Inviting (Sending SMS on Mobile Phones)</strong>
                            </div>
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col col-md-6">                               
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="inviting-delay-min" class="form-control-label">Delay Min</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" id="inviting-delay-min" name="inviting-delay-min" placeholder="Minimal delay" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="inviting-packet-min" class=" form-control-label">Packet Min</label></div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" id="inviting-packet-min" name="inviting-packet-min" placeholder="Minimal Packet" class="form-control" required>
                                            </div>
                                        </div>            
                                    </div>
                                    <div class="col col-md-6">    
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="inviting-delay-max" class=" form-control-label">Delay Max</label></div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" id="inviting-delay-max" name="inviting-delay-max" placeholder="Maximum delay" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="inviting-packet-max" class=" form-control-label">Packet Max</label></div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" id="inviting-packet-max" name="inviting-packet-max" placeholder="Maximum packet" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row mt-2">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header pl-3 pt-0 pb-0 text-left">
                                <strong class="card-title">Messaging (Sending Messages to Telegram Contacts)</strong>
                            </div>
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col col-md-6">                               
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="messaging-delay-min" class="form-control-label">Delay Min</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" id="messaging-delay-min" name="messaging-delay-min" placeholder="Minimal delay" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="messaging-packet-min" class=" form-control-label">Packet Min</label></div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" id="messaging-packet-min" name="messaging-packet-min" placeholder="Minimal Packet" class="form-control" required>
                                            </div>
                                        </div>            
                                    </div>
                                    <div class="col col-md-6">    
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="messaging-delay-max" class=" form-control-label">Delay Max</label></div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" id="messaging-delay-max" name="messaging-delay-max" placeholder="Maximum delay" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="messaging-packet-max" class=" form-control-label">Packet Max</label></div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" id="messaging-packet-max" name="inviting-packet-max" placeholder="Maximum packet" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row mt-2">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header pl-3 pt-0 pb-0 text-left">
                                <strong class="card-title">System (global system settings)</strong>
                            </div>
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col col-md-6">                               
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="system-delay" class="form-control-label">Operation Delay</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" id="system-delay" name="system-delay" placeholder="Delay" class="form-control" required>&nbsp;sec
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row mt-3">
                    <div class="col-md-5">
                        <!--div id="tab-check-phones-process">
                            <span id="message">Check Phones...</span>&nbsp;&nbsp;&nbsp;
                            Total Phones: <span id="total" class="font-weight-bold text-primary">0</span>&nbsp;&nbsp;&nbsp;
                            Current Phone: <span id="current" class="font-weight-bold text-primary">0</span>&nbsp;&nbsp;&nbsp;
                            Telegram: <span id="telegram" class="font-weight-bold text-primary">0</span>
                        </div-->
                    </div>
                    <div class="col-md-7">
                        <div id="part-settings-tab-actions-controls" class="text-right">
                            <span class="btn btn-secondary btn-sm" id="part-settings-tab-actions-btn-defaults">Defaults</span>
                            <span class="btn btn-primary btn-sm" id="part-settings-tab-actions-btn-apply">Apply</span>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        
    <!--/div>
    </div>
</div-->

