<?php if (! defined('ROOT')) exit; ?>
<div class="row">
    <div class="col">
        <table id="tab-data-users-datatable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th></th>
                <th>Phone</th>
                <th>FirstName</th>
                <th>LastName</th>
                <th>Username</th>
                <th>Verified</th>
                <th>UserID</th>
                <th>Hash</th>
                <th>Last</th>
                <th>Sync</th>
                <th>Created</th>
              </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
<div id="tab-data-lists-controls" class="row mt-3">
    <div class="col text-left">
        <span class="btn btn-outline-success btn-sm" id="tab-data-users-btn-info">Get Info</span>
    </div>
    <div class="col text-right">
        <span class="btn btn-secondary btn-sm" id="tab-data-users-btn-delete">Delete</span>
        <span class="btn btn-outline-success btn-sm" id="tab-data-users-btn-reload">Reload</span>
        <span class="btn btn-outline-primary btn-sm" id="tab-data-users-btn-filter">Filter</span>
    </div>
</div>