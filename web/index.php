<?php
/**
 * Site root folder (absolute path of Site directory)
 */
define('ROOT', dirname(__DIR__));

/**
 * Relative document root folder (only name of Document directory with prepended slash)
 */
define('RDROOT', '/web'); // or '' (if Document directory is Site directory)

/**
 * Document root folder (absolute path of Document directory).
 */
define('DROOT', ROOT.RDROOT);

/**
 * URI prefix (is used within layout.php for .css, .js files, image files)
 */
define('URIPREF', ''); // if site will need publish on hosting with DocROOT=ROOT, then it change to '/web'=RDROOT

// Functions
require ROOT . '/lib/functions.php';

// Configs
$config = getConfig();
$sd = $config['system']['delay'];


// Authentication
require ROOT . '/auth/auth.php';


// Routing
require ROOT . '/lib/Router.php';
$router = new Router();

// prepare URI
$req_uri = &$_SERVER['REQUEST_URI'];
$uri = explode('?', $req_uri)[0];

// find handler
if ($router->match($uri)) {
    include $router->path(); // run handler (rendering)
}
