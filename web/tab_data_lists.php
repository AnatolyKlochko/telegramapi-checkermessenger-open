<?php if (! defined('ROOT')) exit; ?>
<div class="row">
    <div class="col">
        <table id="tab-data-lists-datatable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th></th>
              <th>Name</th>
              <th>Telegram</th>
              <th>Unique</th>
              <th>Total</th>
              <th>Uploaded</th>
              <th>Is Telegram Checked</th>
              <th>Checked Date</th>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
    </div>
</div>
<div id="tab-data-lists-controls" class="row mt-3">
    <div class="col text-right">
        <span class="btn btn-secondary btn-sm" id="tab-data-lists-btn-delete">Delete</span>
        <span class="btn btn-outline-success btn-sm" id="tab-data-lists-btn-reload">Reload</span>
        <span class="btn btn-outline-primary btn-sm" id="tab-data-lists-btn-filter">Filter</span>
    </div>
</div>