<?php if (! defined('ROOT')) exit; ?>
<div class="custom-tab">
    <nav>
        <div class="nav nav-tabs" id="nav-data-tab" role="tablist">
            <a class="nav-item nav-link active" id="custom-nav-data-lists-tab" data-toggle="tab" href="#custom-nav-data-lists" role="tab" aria-controls="custom-nav-data-lists" aria-selected="true">Lists</a>
            <a class="nav-item nav-link" id="custom-nav-data-phones-tab" data-toggle="tab" href="#custom-nav-data-phones" role="tab" aria-controls="custom-nav-data-phones" aria-selected="false">Phones</a>
            <a class="nav-item nav-link" id="custom-nav-data-users-tab" data-toggle="tab" href="#custom-nav-data-users" role="tab" aria-controls="custom-nav-data-users" aria-selected="false">Telegram Users</a>
        </div>
    </nav>
    <div class="tab-content pt-2" id="nav-data-tabContent">
        <div class="tab-pane fade show active" id="custom-nav-data-lists" role="tabpanel" aria-labelledby="custom-nav-data-lists-tab">
            <?php include './tab_data_lists.php' ?>
        </div>
        <div class="tab-pane fade" id="custom-nav-data-phones" role="tabpanel" aria-labelledby="custom-nav-data-phones-tab">
            <?php include './tab_data_phones.php' ?>
        </div>
        <div class="tab-pane fade" id="custom-nav-data-users" role="tabpanel" aria-labelledby="custom-nav-data-users-tab">
            <?php include './tab_data_users.php' ?>
        </div>
    </div>
</div>
<!-- Hidden Modal: 'Data' / 'Lists' Delete -->
<div class="hidden">
    <span id="data-lists-modal-delete-btn-control" class="hidden" data-toggle="modal" data-target="#data-lists-modal-delete"></span>
    <div class="modal fade" id="data-lists-modal-delete" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="largeModalLabel">Delete List</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-12">
                            <input type="hidden" data-attr="id" class="form-control" disabled>
                            <input type="text" data-attr="name" placeholder="" class="form-control" disabled>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12"><input type="text" data-attr="telegram" placeholder="" class="form-control" disabled></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12"><input type="text" data-attr="unique" placeholder="" class="form-control" disabled></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12"><input type="text" data-attr="total" placeholder="" class="form-control" disabled></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12"><input type="text" data-attr="uploaded" placeholder="" class="form-control" disabled></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12"><input type="text" data-attr="istelegramchecked" placeholder="" class="form-control" disabled></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12"><input type="text" data-attr="telegramcheckeddate" placeholder="" class="form-control" disabled></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" id="data-lists-modal-delete-btn-delete" class="btn btn-primary">Delete</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Hidden Modal: 'Data' / 'Phones' Delete -->
<div class="hidden">
    <span id="data-phones-modal-delete-btn-control" class="hidden" data-toggle="modal" data-target="#data-phones-modal-delete"></span>
    <div class="modal fade" id="data-phones-modal-delete" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="largeModalLabel">Delete Phone</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-12">
                            <input type="hidden" data-attr="id" class="form-control" disabled>
                            <input type="text" data-attr="number" placeholder="" class="form-control" disabled>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12"><input type="text" data-attr="ischecked" placeholder="" class="form-control" disabled></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12"><input type="text" data-attr="checkeddate" placeholder="" class="form-control" disabled></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12"><input type="text" data-attr="telegram" placeholder="" class="form-control" disabled></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" id="data-phones-modal-delete-btn-delete" class="btn btn-primary">Delete</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Hidden Modal: 'Data' / 'Users' Delete -->
<div class="hidden">
    <span id="data-users-modal-delete-btn-control" class="hidden" data-toggle="modal" data-target="#data-users-modal-delete"></span>
    <div class="modal fade" id="data-users-modal-delete" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="largeModalLabel">Delete User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-12">
                            <input type="hidden" data-attr="id" class="form-control" disabled>
                            <input type="text" data-attr="phone" placeholder="" class="form-control" disabled>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12"><input type="text" data-attr="firstname" placeholder="" class="form-control" disabled></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12"><input type="text" data-attr="lastname" placeholder="" class="form-control" disabled></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12"><input type="text" data-attr="username" placeholder="" class="form-control" disabled></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12"><input type="text" data-attr="verified" placeholder="" class="form-control" disabled></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12"><input type="text" data-attr="userid" placeholder="" class="form-control" disabled></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12"><input type="text" data-attr="hash" placeholder="" class="form-control" disabled></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12"><input type="text" data-attr="last" placeholder="" class="form-control" disabled></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12"><input type="text" data-attr="sync" placeholder="" class="form-control" disabled></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12"><input type="text" data-attr="created" placeholder="" class="form-control" disabled></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" id="data-users-modal-delete-btn-delete" class="btn btn-primary">Delete</button>
                </div>
            </div>
        </div>
    </div>
</div>