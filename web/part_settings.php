<?php if (! defined('ROOT')) exit; ?>
<!-- Part 'Settings' -->
<div class="row">
    <div class="col-lg-12">
        <!--div class="card">
            <div class="card-header">
            </div>
            <div class="card-body"-->
                <div class="custom-tab">
                    <nav>
                        <div class="nav nav-tabs" id="nav-settings-tab" role="tablist">
                            <a class="nav-item nav-link active" id="custom-nav-settings-actions-tab" data-toggle="tab" href="#custom-nav-settings-actions" role="tab" aria-controls="custom-nav-settings-actions" aria-selected="true">Application</a><!-- Actions -->
                            <a class="nav-item nav-link" id="custom-nav-settings-telegramaccounts-tab" data-toggle="tab" href="#custom-nav-settings-telegramaccounts" role="tab" aria-controls="custom-nav-settings-telegramaccounts" aria-selected="false">Telegram</a><!-- Telegram Accounts -->
                            <a class="nav-item nav-link" id="custom-nav-appusers-tab" data-toggle="tab" href="#custom-nav-appusers" role="tab" aria-controls="custom-nav-appusers" aria-selected="false">Security</a><!-- Application Users -->
                        </div>
                    </nav>
                    <div class="tab-content pt-2" id="nav-settings-tabContent">
                        <div class="tab-pane fade show active" id="custom-nav-settings-actions" role="tabpanel" aria-labelledby="custom-nav-settings-actions-tab">
                            <?php include './part_settings_tab_actions.php' ?>
                        </div>
                        <div class="tab-pane fade" id="custom-nav-settings-telegramaccounts" role="tabpanel" aria-labelledby="custom-nav-settings-telegramaccounts-tab">
                            <?php include './part_settings_tab_telegramaccounts.php' ?>
                        </div>
                        <div class="tab-pane fade" id="custom-nav-appusers" role="tabpanel" aria-labelledby="custom-nav-appusers-tab">
                            <?php include './part_settings_tab_appusers.php' ?>
                        </div>
                    </div>
                </div>
            <!--/div>
        </div-->
    </div>
</div>