<?php if (! defined('ROOT')) exit; ?>
<div class="card-body card-block">
    <form action="" method="post" enctype="multipart/form-data" class="form-horizontal" id="tab-upload-form">
        <div class="row form-group">
            <div class="col col-md-3"><label for="phone-list" class=" form-control-label">Phone's List Name</label></div>
            <div class="col-12 col-md-9">
                <input type="text" id="phone-list" name="phone-list" placeholder="Text" class="form-control" required>
                <small class="form-text text-muted">Give a logic name, some like <i>'2018.02.15 Satuday Event. Eco Plaza.'</i></small>
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="phones" class=" form-control-label">Phones File</label></div>
            <div class="col-12 col-md-9">
                <input type="file" id="phones" name="phones" class="form-control-file" accept="text/plain" required>
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-md-6">
                <div id="tab-upload-form-process">
                    <div class="row">
                        <div class="col col-md-4 text-left">info:</div>
                        <div class="col col-md-8 text-left text-primary" id="info"></div>
                    </div>
                    <div class="row">
                        <div class="col col-md-4 text-left">action:</div>
                        <div class="col col-md-8 text-left text-secondary" id="action"></div>
                    </div>
                    <div class="row">
                        <div class="col col-md-4 text-sm-left">started:</div>
                        <div class="col col-md-8 text-left text-secondary" id="start"></div>
                    </div>
                    <div class="row">
                        <div class="col col-md-4 text-left">in:</div>
                        <div class="col col-md-8 text-left text-primary"><span id="passed"></span></div>
                    </div>
                    <div class="row">
                        <div class="col col-md-4 text-left">finished:</div>
                        <div class="col col-md-8 text-left text-secondary" id="end"></div>
                    </div>
                    <div class="row">
                        <div class="col col-md-4 text-left">total:</div>
                        <div class="col col-md-8 text-left text-secondary"><span id="total"></span></div>
                    </div>
                    <div class="row">
                        <div class="col col-md-4 text-left">current:</div>
                        <div class="col col-md-8 text-left text-primary"><span id="current"></span></div>
                    </div>
                    <div class="row">
                        <div class="col col-md-4 text-left">left:</div>
                        <div class="col col-md-8 text-left text-secondary"><span id="left"></span></div>
                    </div>
                    <div class="row">
                        <div class="col col-md-4 text-left">unique:</div>
                        <div class="col col-md-8 text-left text-primary"><span id="unique"></span></div>
                    </div>
                </div>
            </div>
            <div class="col col-md-6 text-right">
                <button type="submit" class="btn btn-primary btn-sm" id="upload-phones">
                    <i class="fa fa-upload"></i> Upload
                </button>
            </div>
        </div>
    </form>
</div>
<!--div class="card-footer ">
</div-->
