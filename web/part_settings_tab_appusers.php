<?php if (! defined('ROOT')) exit; ?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <table id="part-settings-tab-appusers-datatable" class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Login</th>
                      <th>Password</th>
                      <th>Salt</th>
                      <th>Created</th>
                      <th>Enter</th>
                      <th>Account</th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-md-12">
        <div id="part-settings-tab-appusers-controls" class=" text-right">
            <span class="btn btn-secondary btn-sm" id="part-settings-tab-appusers-delete" data-toggle="modal" data-target="#appusers-modal-delete">Delete</span>
            <span class="btn btn-outline-success btn-sm ml-1" id="part-settings-tab-appusers-update" data-toggle="modal" data-target="#appusers-modal-update">Update</span>
            <span class="btn btn-primary btn-sm ml-1" id="part-settings-tab-appusers-add" data-toggle="modal" data-target="#appusers-modal-add">Add</span>
        </div>
    </div>
    
</div>