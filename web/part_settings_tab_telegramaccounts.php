<?php if (! defined('ROOT')) exit; ?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <table id="part-settings-tab-telegramaccounts-datatable" class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Phone</th>
                      <th>Username</th>
                      <th>Created</th>
                      <th>Status</th>
                      <th>ApiID (api_id)</th>
                      <th>ApiHASH (api_hash)</th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col col-6 text-left">
        <span class="btn btn-outline-success btn-sm" id="part-settings-tab-telegramaccounts-as-default" style="width: 150px;">As Default Account</span>
    </div>
    <div class="col col-6 text-right">
        <span class="btn btn-secondary btn-sm" id="part-settings-tab-telegramaccounts-delete" style="width: 130px;" data-toggle="modal" data-target="#telegramaccounts-modal-delete">Delete</span>
        <span class="btn btn-outline-success btn-sm ml-1" id="part-settings-tab-telegramaccounts-update" style="width: 130px;" data-toggle="modal" data-target="#telegramaccounts-modal-update">Update</span>
        <span class="btn btn-primary btn-sm ml-1" id="part-settings-tab-telegramaccounts-add" style="width: 130px;" data-toggle="modal" data-target="#telegramaccounts-modal-add">Add</span>
    </div>
</div>