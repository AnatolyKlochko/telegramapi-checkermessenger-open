<?php
/**
 * A simple router.
 *
 * @author Anatoly Klochko
 */
class Router
{
    private $routes;
    private $result;
    private $path;
    
    public function __construct()
    {
        $routes = include ROOT.'/config/routes.php';
        if (! is_array($routes) || count($routes) < 1) {
            throw new Exception('No routes file.');
        }
        
        $this->routes = &$routes;
    }
    
    public function match(string &$uri) : bool
    {
        $rel_path = &$this->routes[$uri];
        $this->result = (! empty($rel_path));
        
        if ($this->result) {
            $this->path = ROOT . $rel_path;
            if (! file_exists($this->path)) {
                throw new Exception('No handler file (\'' . $this->path . '\').');
            }
        }
        
        return $this->result;
    }
    
    public function path()
    {
        return $this->path;
    }
}
