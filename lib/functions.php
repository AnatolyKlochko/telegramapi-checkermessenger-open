<?php if (! defined('ROOT')) exit;


/*   Functions   */



/*   S T R I N G S   */

/**
 * 
 */
function deleteEndStrIf(string $str = '', string $del) : string
{
    // Prepare value
    $del = str_replace(' ', '\s', $del);
    // Deleting
    if (preg_match("/$del$/", $str) === 1) {
        $str = preg_replace("/$del$/", '', $str);
    }
    
    return $str;
}



/*    A   R   R   A   Y   S    */
function cleanArray(string &$arr, $delimiter) : array
{
    $i_arr = explode($delimiter, $arr);
    foreach ($i_arr as $key => &$val) {
        if (empty($val)) {
            unset($i_arr[$key]);
            continue;
        }
        $val = trim($val);
    }
    return $i_arr;
}



/*   M I S C   */

/**
 * 
 */
function saveArrayAsJsonFile(string $path, array &$arr, bool $is_readable = true)
{
    $output = ($is_readable) ? json_encode($arr, JSON_PRETTY_PRINT) : json_encode($arr);
    if (json_last_error() === 0) {
        file_put_contents($path, $output);
        return true;
    }
    return false;
}

/**
 * 
 */
function getJsonFileAsArray(string $path)
{
    if (is_file($path)) {
        $arr = json_decode(file_get_contents($path), true);
        if (json_last_error() === 0) {
            return $arr;
        }
    }
    return false;
}
/**
 * 
 */
function prepJson(string &$str)
{
    $str = utf8_encode($str);
    str_replace("\x92", "'", $str);
}



/*   A P P L I C A T I O N   */
/**
 * 
 */

/**
 * 
 */
function getConfig(string $area = '', bool $update = false)
{
    static $config = [];
    // Initialize at once
    if (empty($config)) {
        $config = getJsonFileAsArray(ROOT . '/config/app.json');
    }
    
    // Reload configs
    if ($update) {
        $config = getJsonFileAsArray(ROOT . '/config/app.json');
        $GLOBALS['config'] = &$config;
    }
    
    switch ($area) {
        case 'user':
            return $config['access']['user'];
        
        case 'cookie':
            return $config['access']['cookie'];

        default:
            return $config;
    }
    
}
function saveConfig(array &$config, string $area = null)
{
    if (empty($area)) {
        saveArrayAsJsonFile(ROOT . '/config/app.json', $config);
    } else {
        $configtotal = getJsonFileAsArray(ROOT . '/config/app.json');
        switch ($area) {
            case 'user':
                $configtotal['access']['user'] = $config;
                break;
            case 'cookie':
                $configtotal['access']['cookie'] = $config;
                break;
        }
        saveArrayAsJsonFile(ROOT . '/config/app.json', $configtotal);
    }
}

/**
 * 
 */
function getSchemaConfig(string &$conn_name = 'default')
{
    $schema_config = getJsonFileAsArray(ROOT . '/config/schema.json');
    $schema = $schema_config[$conn_name];
    if (empty($schema)) {
        throw new \LogicException('Absent database connection.');
    }
    return $schema;
}
/**
 * 
 */
function getDbHandler(string $conn_name = 'default')
{
    static $dbh;
    // Saved object
    if (!empty($dbh)) {
        return $dbh;
    }
    
    // Database configuration
    $schema_config = getSchemaConfig($conn_name);
    
    // Try to create database handler
    try {
        $dbh = new PDO("mysql:host={$schema_config['host']};dbname={$schema_config['db']}", $schema_config['user'], $schema_config['pass']);
    } catch (PDOException $e) {
        stop(false, "MySQL error: " . $e->getMessage() . "<br/>");
    }
    
    // Save object to global
    if (!isset($GLOBALS['dbh'])) {
        $GLOBALS['dbh'] = $dbh;
    }
    
    // Get result
    return $dbh;
}
function getNewDbHandler(string $conn_name = 'default')
{
    // Database configuration
    $schema_config = getSchemaConfig($conn_name);
    
    // Try to create database handler
    try {
        $dbh = new PDO("mysql:host={$schema_config['host']};dbname={$schema_config['db']}", $schema_config['user'], $schema_config['pass']);
    } catch (PDOException $e) {
        stop(false, "PDO exception (MySQL error): " . $e->getMessage() . "<br/>");
    }
        
    // Get result
    return $dbh;
}

/**
 * 
 */
function user()
{
    if (isset($GLOBALS['user'])) {
        return $GLOBALS['user'];
    }
    return false;
}


/**
 * 
 */
function getDefaultAccountPhone()
{
    $config = getConfig();
    
    // Default phone
    return $config['account']['default'];
}
/**
 * 
 */
function getAccountPhones() : array
{
    $config = getConfig();
    
    $phones = [];
    
    // x No Default phone in list    
    
    // Next phones
    foreach ($config['account']['data'] as $phone => $account_options) {
        $phones[] = $phone;
    }
    
    // Get result
    return $phones;
}

/**
 * 
 */
function getCurrentPhone()
{
    $phone = '';
    if (isset($_POST['phone'])) {
        $phone = $_POST['phone'];
        if (verifyPhone($phone)) {
            return $phone;
        }
        stop(false, 'Phone number is invalid.');
    }
    
    // Phone of current user
    $user = user();
    if ($user !== false) {
        $users = getConfig('user');
        $phone = $users[$user]['account'];
        if (verifyPhone($phone)) {
            return $phone;
        }
        //stop(false, 'Phone number is invalid.');
    }
    
    return null;
}

/**
 * 
 */
function stop(bool $result, string $message = '', $data = null)
{
    outputResponse($result, $message, $data);
    exit;
}

/**
 * 
 */
function outputResponse($result, $message = null, $data = null)
{
    $response = [
        'result' => $result,
        'message' => $message,
        'data' => $data
    ];
    echo json_encode($response, JSON_PRETTY_PRINT);
}


/**   TABS   */

/** Login */

/**
 * 
 */
function verifyCode(&$code) : bool
{
    if (preg_match('~^[0-9]{5,10}$~', $code) === 1) {
        return true;
    }
    return false;
}

/**
 * Matches latin and unicode letters (pattern '\p{L}' with 'u' modifier).
 */
function verifyActionName(&$name) : bool
{
    if (preg_match('~^[\-\p{L}]{2,45}$~u', $name) === 1) {
        return true;
    }
    return false;
}

/** Upload */

function extractPhone(string &$raw_phone) : string
{    
    $phone_number = '';
    for ($i = 0, $len = strlen($raw_phone); $i < $len; $i++) {
        $phone_number .= preg_match('~^[0-9]$~', $raw_phone[$i]) === 1 ? $raw_phone[$i] : '';
    }
    
    return $phone_number;
}

/**
 * 
 */
function verifyPhone(&$phone) : bool
{    
    if (preg_match('~^[0-9]{8,25}$~', $phone) === 1) {
        return true;
    }
    return false;
}
