# Telegram API

It is an example of a real project from Telegram API shere.

![PHP from Travis config](https://img.shields.io/travis/php-v/symfony/symfony.svg?style=for-the-badge)

## Description

Application allows to check mobile phone numbers on *Telegram* and perform messaging (sending). As data store is used MySQL schema (but it can be any another data store type).

For commercial reason in project source files was deleted code that is belongs to working with *Telegram API*. But below You can find amazing guide that describes a whole thing of working with *Telegram API*. In that guide You will see few examples of how can be used *Telegram* with its API, and that is demonstrated only very little part.

## Business
If you have a project with attractive commercial offer - wellcome, I am able implement any Your ideas (sometimes it's only a time question). For example, I can create smart *business bot* for Your company, wich will do something very helpful for Your business or something else that allows to do *Telegram API*.

## Screenshots

There is a whole guide that demonstrates of how Application works and how to connect and use *Telegram API*. Guide is placed at Google Fotos, just visit [project album](https://photos.app.goo.gl/sVNSsDWZjVEHgw7FA) and run slideshow (at top right corner, vertical three dots, *Slideshow* menu item).