<?php if (! defined('ROOT')) exit; ?>
<style>
input[type=text], input[type=password] {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
}
button {
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
}

button:hover {
    opacity: 0.8;
}
.container {
    width: 300px;
    padding: 16px;
    margin: 0 auto;
}
</style>

<form method="POST">
    <div class="container">
        <label><b>Login</b></label>
        <input type="text" placeholder="Enter Login" name="login" required>

        <label><b>Password</b></label>
        <input type="password" placeholder="Enter Password" name="password" required>
        
        <label style="color:red;"><?= (isset($_POST['login']) && isset ($_POST['password'])) ? '*incorrect data' : '' ?></label>
        
        <button type="submit">Login</button>
    </div>
</form>
