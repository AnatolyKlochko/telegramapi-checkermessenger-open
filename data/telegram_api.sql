SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `telegram_api`
--
CREATE DATABASE IF NOT EXISTS `telegram_api` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `telegram_api`;

-- --------------------------------------------------------

--
-- Table structure for table `inviting`
--

DROP TABLE IF EXISTS `inviting`;
CREATE TABLE IF NOT EXISTS `inviting` (
  `PhoneListID` smallint(8) UNSIGNED NOT NULL COMMENT 'ID of a phone list from `PhoneList` table',
  `LastUserID` int(8) UNSIGNED NOT NULL COMMENT 'ID of a Telegram User from `user` table (it IS NOT telegramID of User'),
  PRIMARY KEY (`PhoneListID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `messaging`
--

DROP TABLE IF EXISTS `messaging`;
CREATE TABLE IF NOT EXISTS `messaging` (
  `PhoneListID` smallint(8) UNSIGNED NOT NULL COMMENT 'ID of a phone list from `PhoneList` table',
  `LastUserID` int(8) UNSIGNED NOT NULL COMMENT 'ID of a Telegram User from `user` table (it IS NOT telegramID of User'),
  PRIMARY KEY (`PhoneListID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `phone`
--

DROP TABLE IF EXISTS `phone`;
CREATE TABLE IF NOT EXISTS `phone` (
  `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `PhoneListID` smallint(5) UNSIGNED NOT NULL,
  `Number` char(25) NOT NULL,
  `IsTelegramChecked` tinyint(1) NOT NULL DEFAULT '0',
  `TelegramCheckedDate` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `HasTelegramAccount` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Number` (`Number`),
  KEY `ListID` (`PhoneListID`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `phonelist`
--

DROP TABLE IF EXISTS `phonelist`;
CREATE TABLE IF NOT EXISTS `phonelist` (
  `ID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Name` char(100) NOT NULL COMMENT 'List name',
  `Unique` mediumint(8) UNSIGNED NOT NULL COMMENT 'Count of Unique Phones',
  `Total` mediumint(8) UNSIGNED NOT NULL COMMENT 'Total Count of Phones (unique and not)',
  `Uploaded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'When list was uploaded',
  `IsTelegramChecked` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Was list checked on Telegram?',
  `TelegramCheckedDate` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `TelegramAccounts` mediumint(8) NOT NULL DEFAULT '0' COMMENT 'Count of Telegram Accounts in List',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Name` (`Name`,`Uploaded`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Triggers `phonelist`
--
DROP TRIGGER IF EXISTS `DeleteFromInvitingMessaging`;
DELIMITER $$
CREATE TRIGGER `DeleteFromInvitingMessaging` AFTER DELETE ON `phonelist` FOR EACH ROW BEGIN
    DELETE FROM inviting WHERE PhoneListID = OLD.ID;
    DELETE FROM messaging WHERE PhoneListID = OLD.ID;
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `InsertToInvitingMessaging`;
DELIMITER $$
CREATE TRIGGER `InsertToInvitingMessaging` AFTER INSERT ON `phonelist` FOR EACH ROW BEGIN
    INSERT INTO inviting SET PhoneListID = NEW.ID, LastUserID = 0;
    INSERT INTO messaging SET PhoneListID = NEW.ID, LastUserID = 0;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `telegramuser`
--

DROP TABLE IF EXISTS `telegramuser`;
CREATE TABLE IF NOT EXISTS `telegramuser` (
  `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `PhoneListID` smallint(5) UNSIGNED NOT NULL,
  `UserID` char(20) NOT NULL COMMENT 'The User ID in Telegram, is used for sending invites and messages',
  `AccessHash` char(64) DEFAULT NULL,
  `Username` char(30) DEFAULT NULL COMMENT 'The User Login in Telegram',
  `FirstName` char(15) DEFAULT NULL,
  `LastName` char(15) DEFAULT NULL,
  `Phone` char(25) DEFAULT NULL COMMENT 'The Mobile Phone of User that is used while goes registration in Telegram',
  `Verified` tinyint(1) NOT NULL,
  `WasOnline` timestamp NULL DEFAULT NULL COMMENT 'When User was in Telegram at last time',
  `IsSynchronized` tinyint(1) NOT NULL,
  `Created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UserID` (`UserID`),
  KEY `PhoneListID_ID_UserID` (`PhoneListID`,`ID`,`UserID`) USING BTREE,
  KEY `PhoneListID_ID_Phone` (`PhoneListID`,`ID`,`Phone`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
