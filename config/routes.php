<?php if (! defined('ROOT')) exit;
/**
 * pathes (right side) is fit to ROOT
 */
return [
    // index
    '/'                               => RDROOT.'/layout.php',
        
    // Misc
    '/action/is-logged'               => '/action/is-logged.php',
    '/action/logout'                  => '/action/logout.php',
    '/action/application-logout'      => '/action/application-logout.php',
    '/action/mark-account-as-blocked' => '/action/mark-account-as-blocked.php',
    '/action/get-config'              => '/action/get-config.php',
    '/action/get-account-phones'      => '/action/get-account-phones.php',
    '/action/get-phones-list'         => '/action/get-phones-list.php',
    
    // 'Login' tab
    '/action/login-code'              => '/action/login-code.php',
    '/action/login'                   => '/action/login.php',
    
    // 'Upload Phones' tab
    '/action/upload-phones'           => '/action/upload-phones.php',
    '/action/stats'                   => '/action/stats.php',
    
    // 'Data' tab
    
    // 'Data' tab, 'Lists' subtab
    '/action/get-data-lists'          => '/action/get-data-lists.php',
    '/action/data-list-delete'        => '/action/data-list-delete.php',
    
    // 'Data' tab, 'Phones' subtab
    '/action/get-data-phones'         => '/action/get-data-phones.php',
    '/action/data-phone-delete'       => '/action/data-phone-delete.php',
    
    // 'Data' tab, 'Telegram Users' subtab
    '/action/get-data-users'          => '/action/get-data-users.php', // 'Reload' button
    '/action/data-user-info'          => '/action/data-user-info.php', // 'Get Info' button
    '/action/data-user-delete'        => '/action/data-user-delete.php', // 'Delete' button
    
    // 'Checking' tab
    '/action/mark-as-unchecked'       => '/action/mark-as-unchecked.php',
    '/action/get-checking-lists'      => '/action/get-checking-lists.php',
    '/action/checking-start'          => '/action/checking-start.php',
    
    // 'Inviting' tab
    '/action/get-inviting-lists'      => '/action/get-inviting-lists.php',
    '/action/inviting-start'          => '/action/inviting-start.php',
    
    // 'Messaging' tab
    '/action/get-messaging-lists'     => '/action/get-messaging-lists.php',
    '/action/messaging-start'         => '/action/messaging-start.php',
    
    
    // Settings
    
    // 'Application' tab
    '/action/set-action-defaults'     => '/action/set-action-defaults.php',
    '/action/get-action-settings'     => '/action/get-action-settings.php',
    '/action/set-action-settings'     => '/action/set-action-settings.php',
    
    // 'Telegram Accounts' tab
    '/action/get-telegram-accounts'   => '/action/get-telegram-accounts.php',
    '/action/telegram-account-add'    => '/action/telegram-account-add.php',
    '/action/telegram-account-update' => '/action/telegram-account-update.php',
    '/action/telegram-account-delete' => '/action/telegram-account-delete.php',
    
    // 'Application Users' tab
    '/action/app-user-add'            => '/action/app-user-add.php',
    '/action/app-user-update'         => '/action/app-user-update.php',
    '/action/app-user-delete'         => '/action/app-user-delete.php',
    
    // Testing
    '/action/test'                    => '/action/test.php',
];